#!/bin/bash
#
# Copyright (C) 2022 Pablo Correa Gomez
#

set -e

source ./packages

die () {
    echo $1
    return 1
}

# Missing:
# * orca has tags of the form ORCA_42_3
ver_fix () {
    local pkg="$1"
    local input="$2"
    local output="${input}"

    if [ "$package" = "gnome-maps" ] ||
	   [ "$package" = "calls" ]
    then
	output="v${input}"
    fi

    echo "${output}"
}

test -n "$1" || die "A first argument with the major version to update is required"
MAJ_VER="$1"

PACKAGES="$GNOME_CORE_APPS $GNOME_SYSTEM"
APORTSDIR=${APORTSDIR:-"$HOME/aports"}
test -d $APORTSDIR || die "A directory is needed, $APORTSDIR does not exist "

BRANCH="${2:-"master"}"
GIT="git --git-dir=${APORTSDIR}/.git --work-tree=${APORTSDIR}"
ORIGINAL_BRANCH="$($GIT rev-parse --abbrev-ref HEAD)"
$GIT checkout --quiet $BRANCH
$GIT checkout --quiet -B gnome-$MAJ_VER-$BRANCH

GNOME_API_NAMESPACE="https://gitlab.gnome.org/api/v4/projects/GNOME%2F"
TAGS_API_ENDPOINT="/repository/tags"
for package in $PACKAGES; do
    if [ "$package" = "gnome-software" ]; then
	echo "skipping gnome-software, needs plugin sync"
	continue
    fi

    upstream_name="$package"
    # These packages might be renamed
    if [ "$package" = "gnome-console" ]; then
	upstream_name="console"
    fi

    search_query="$(ver_fix "$package" "${MAJ_VER}")"

    VER="$(curl --silent \
	       "${GNOME_API_NAMESPACE}${upstream_name}${TAGS_API_ENDPOINT}?search=^${search_query}" |
	      jq -r '.[0].name')"
    VER=${VER#v}

    cd $APORTSDIR/community/$package
    . APKBUILD
    # orca has tags of the form ORCA_42_3
    # gnome-maps has tags of the form v42.3
    if [ -z "$VER" ] || [ x"$VER" = x"null" ] || [ "$VER" = "$pkgver" ]; then
	echo "skipped $package, latest version: $VER, packaged version: $pkgver"
	continue
    fi

    # Copied from abump. Abump tries to build every package,
    # which might not be desired
    sed -i -e "s/^pkgver=.*/pkgver=$VER/" \
	-e "s/^pkgrel=.*/pkgrel=0/" \
	APKBUILD

    abuild checksum

    git add APKBUILD
    git commit -m"community/$package: upgrade to $VER"
    cd -
done


$GIT checkout --quiet $ORIGINAL_BRANCH
